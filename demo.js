function max(a,b)
{
	if(a==b)
		return a;
	else{
		if(a>b)
			return a;
		else
			return b;
	}
}


function findPlatform( arrival, departure, n)
{

	var plat_req = 1, result = 1;
	var i = 1, j = 0;

	for (var i = 0; i < n; i++) {
		plat_req = 1;

		for (var j = i + 1; j < n; j++) {
			if ((arrival[i] >= arrival[j] && arrival[i] <= departure[j]) || (arrival[j] >= arrival[i] && arrival[j] <= departure[i]))
				plat_req++;
		}

		result = max(result, plat_req);
	}

	return result;
}

var arrival = [ 1300, 1400, 1530, 1700 ];
var departure = [ 1330, 1410, 1800, 1830 ];
var n =6;
console.log("Minimum Number of Platforms Required = "+findPlatform(arrival, departure, n));



